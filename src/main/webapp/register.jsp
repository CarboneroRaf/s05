<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirm Registration</title>
</head>
<body>

	<%
		String discovery = session.getAttribute("discovery").toString();
		String message;
		
		if(discovery.equals("friends")){
			discovery="Friends";
		} else if(discovery.equals("social")){
			discovery="Social Media";
		} else {
			discovery="Others";
		}
		
		String dateofbirth = session.getAttribute("dateofbirth").toString();
		dateofbirth = dateofbirth.replace("T", " / ");
	%>
	
	<h1>Booking Confirmation</h1>
	<p>First Name: <%=session.getAttribute("firstname") %></p>
	<p>Last Name: <%=session.getAttribute("lastname") %></p>
	<p>Phone Number: <%=session.getAttribute("phone") %></p>
	<p>Email: <%=session.getAttribute("email") %></p>
	<p>App Discovery: <%= discovery %></p>
	
	<p>Date of Birth: <%= dateofbirth %></p>
	<p>Status: <%= session.getAttribute("usertype") %></p>
	<p>Description: <%= session.getAttribute("description") %></p>
	
	<form action="home.jsp" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>

</body>
</html>