<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome</title>
</head>
<body>

	<h1>Welcome <%=session.getAttribute("firstname")%> <%=session.getAttribute("lastname")%>!</h1>
	
	<p>
		<%
			String usertype = session.getAttribute("usertype").toString();
			
			if(usertype.equals("employer")){
				out.println("Welcome employer. You may now start browsing applicant profiles.");
			} else {
				out.println("Welcome applicant. You may now start looking for your career opportunity.");
			}
		%>
	</p>

</body>
</html>