<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Job Finder</title>
</head>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form method="post" action="register">
		<div>
			<label for="firstname">First Name</label>
			<input type="text" name="firstname" required>
		</div>
		
		<div>
			<label for="lastname">Last Name</label>
			<input type="text" name="lastname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="discovery" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="social" name="discovery" required value="social">
			<label for="social">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="discovery" required value="others">
			<label for="others">Others</label>
			<br>
		</fieldset>
		
		
		
		<div>
			<label for="dateofbirth">Date of Birth</label>
			<input type="date" name="dateofbirth" required>
		</div>
		
		<div>
			<label for="usertype">Are you an employer or applicant?</label>
			<select id="usertype" name="usertype">
				<option value="" selected="selected"> Select one </option>
				<option value="employer"> Employer </option>	
				<option value="applicant"> Applicant </option>
			</select>
		</div>
		
		<div>
			<label for="description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		
		<button>Register</button>
		
	</form>
</body>
</html>