package com.zuitt;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
ArrayList<String> register = new ArrayList<>();
	
	private int registerCount;
	
	public void init() throws ServletException{
		System.out.println("LoginServlet has been initizalized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//Generatess current date and time
		String data = LocalTime.now().toString();
		
		//Increase the booking count by one to be used for the order number
		registerCount++;
		
		//Generate custom booking number with booking count and data
		String orderNumber = registerCount + data.replaceAll("[^a-zA-z0-9]+", "");
		
		//Add the geenrated order number to the arrayList/database
		register.add(orderNumber);
		
		//Add the bookings to be retrieved in all servlets
		HttpSession session = req.getSession();
		session.setAttribute("register", register);
		
		res.sendRedirect("register.jsp");
		}
	
		public void destroy() {
			System.out.println("LoginServlet has been finalaized.");	
		}

}
